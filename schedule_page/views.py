from django.shortcuts import render, redirect
from .forms import FormSchedule
from .models import Schedule

def schedule_page(request):
    schedules = Schedule.objects.all()
    return render(request, 'schedule_page.html', {'sched': schedules})

def addschedule_page(request):
    form = FormSchedule()
    if request.method == "POST":
        form = FormSchedule(request.POST)
        if form.is_valid():
            form.save()
        return redirect('schedule_page:add')
    return render(request, 'addschedule_page.html', {'form' : form})

def deleteschedule(request, id):
    deleted = Schedule.objects.get(id=id)
    deleted.delete()
    newsched = Schedule.objects.all()

    return render(request, 'schedule_page.html', {'sched' : newsched})