from django.urls import path
from . import views

app_name = 'schedule_page'

urlpatterns = [
    path('', views.schedule_page),
    path('add', views.addschedule_page, name='add'),
    path('delete/<id>', views.deleteschedule, name='delete')
]