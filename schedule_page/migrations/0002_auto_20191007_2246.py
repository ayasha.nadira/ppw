# Generated by Django 2.1.1 on 2019-10-07 15:46

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedule_page', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='schedule',
            old_name='date',
            new_name='tanggal',
        ),
    ]
