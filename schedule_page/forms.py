from django.db import models
from django import forms
from . import models
import datetime

class FormSchedule(forms.ModelForm):
    kategori = forms.CharField(widget=forms.TextInput(attrs = {"placeholder" : "Kategori Jadwal" }))
    nama = forms.CharField(widget=forms.TextInput(attrs = {"placeholder" : "Nama Jadwal" }))
    tempat = forms.CharField(widget=forms.TextInput(attrs = {"placeholder" : "Nama Tempat" }))
    tanggal = forms.DateField(initial=datetime.date.today(),
                widget=forms.TextInput(attrs={'class':'form-control'}))
    jam =  forms.TimeField(initial=datetime.time(),
                widget=forms.TextInput(attrs={'class':'form-control'}))
                        
    class Meta:
        model = models.Schedule
        fields = [
                'kategori',
                'nama',
                'tempat',
                'tanggal',
                'jam']