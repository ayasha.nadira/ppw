from django.db import models

# Create your models here.
class Schedule(models.Model):
    nama = models.CharField(max_length=100)
    kategori = models.CharField(max_length=50)
    tempat = models.CharField(max_length=75)
    tanggal = models.DateField()
    jam = models.TimeField()