from django.apps import AppConfig


class FunfactPageConfig(AppConfig):
    name = 'funfact_page'
