from django.urls import path
from . import views

urlpatterns = [
    path('', views.funfact_page, name='funfact_page')
]
