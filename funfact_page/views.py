from django.shortcuts import render

def funfact_page(request):
    return render(request, 'funfact_page.html')